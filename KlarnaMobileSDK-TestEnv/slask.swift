//
//  ViewController.swift
//  AlfaGoOn-iOS
//
//  Created by Amy Niklasson on 2018-11-29.
//  Copyright © 2018 Amy Niklasson. All rights reserved.
//

import UIKit
import WebKit
import KlarnaMobileSDK


/*class slask: UIViewController, WKScriptMessageHandler, WKUIDelegate, WKNavigationDelegate, KlarnaHybridEventListener {

    
    func klarnaWillShowFullscreen(inWebView webView: KlarnaWebView, completionHandler: @escaping () -> Void) {
        print("klarnaWillShowFullscreen")
    }
    
    func klarnaDidShowFullscreen(inWebView webView: KlarnaWebView, completionHandler: @escaping () -> Void) {
        print("klarnaDidShowFullscreen")
    }
    
    func klarnaWillHideFullscreen(inWebView webView: KlarnaWebView, completionHandler: @escaping () -> Void) {
        print("klarnaWillHideFullscreen")
    }
    
    func klarnaDidHideFullscreen(inWebView webView: KlarnaWebView, completionHandler: @escaping () -> Void) {
        print("klarnaDidHideFullscreen")
    }
    
    func klarnaFailed(inWebView webView: KlarnaWebView, withError error: KlarnaMobileSDKError) {
        print("klarnaFailed")
    }
    

    

   // @IBOutlet weak var WebViewContainer: UIView!
    
    @IBOutlet weak var WebViewContainer: UIView!
    
    
    var logicWebView : WKWebView!
    
    var floatRefreshButton:UIButton!
    var latitude : Double = 200.0 // value that latitude should never get
    var longitude : Double = 200.0 // value that longitude should never get
    let uid = UIDevice.current.identifierForVendor?.uuidString
    let url = URL (string: "https://app.klt.kodsmedjan.se/GoOn/amybeta/ticket.php")
    var webReady = false
    var appInBackground = false
    var viewShowing = true
    /*KLARNA TEST*/
    var klarnaSDK:KlarnaHybridSDK!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //To hide tab-bar when testing the swipe-up menu
        //self.tabBarController?.tabBar.isHidden = true
        
        //SETUP CONTENTCONTROLLER (Connection between swift, webview and webpage)
        let contentController = WKUserContentController()
        contentController.add(self, name: "serviceRequest")
        let config = WKWebViewConfiguration()
        config.userContentController = contentController
        
        //SETUP WKWEBVIEW
        logicWebView = WKWebView(frame: self.view.frame, configuration: config)
        
        //Required for webView to work at all
        logicWebView.uiDelegate = self
        //Required for func webView(_ webView: WKWebView, didFinish navigation:WKNavigation!)
        
        logicWebView.navigationDelegate = self
     
        WebViewContainer.addSubview(logicWebView)
        /*Constraints for logicWebView*/
        logicWebView.translatesAutoresizingMaskIntoConstraints = false
             NSLayoutConstraint.activate([
             logicWebView.topAnchor.constraint(equalTo: WebViewContainer.topAnchor),
             logicWebView.leadingAnchor.constraint(equalTo: WebViewContainer.leadingAnchor),
             logicWebView.trailingAnchor.constraint(equalTo: WebViewContainer.trailingAnchor),
             logicWebView.bottomAnchor.constraint(equalTo: WebViewContainer.bottomAnchor),
                //logicWebView.bottomAnchor.constraint(equalTo: WebViewContainer.safeAreaInsets.bottom),
              ])
        print("SAFEAREAINSETS")
        print(WebViewContainer.bottomAnchor)
        print(WebViewContainer.safeAreaInsets.bottom)
        logicWebView.scrollView.bounces = true;
        //logicWebView.scrollView.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: tabBarController!.tabBar.frame.height, right: 0.0)
        

        /*KLARNA TEST*/
        KlarnaMobileSDKCommon.setLoggingLevel(.verbose)
        klarnaSDK = KlarnaHybridSDK(returnUrl: URL(string: "kltgoon://")!, eventListener: self )
         //klarnaSDK = KlarnaHybridSDK(webView: logicWebView, returnUrl: url!, eventListener: self as KlarnaHybridSDKEventListener )
        klarnaSDK.addWebView(self.logicWebView)

        /*KLARNA TEST END*/
        
        //To call the url without using any cache. To use cache change to: URLRequest(url: url!)
        var urlRequest = URLRequest.init(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 100)
        urlRequest.httpMethod = "PUT"
        let postString = "id=\( uid ?? "")"
        urlRequest.httpBody = postString.data(using: .utf8)
        
        logicWebView.load(urlRequest)
        

    }
    
    
    //TRIGGERED WHEN RECEIVED TRIGGER FROM WEBVIEW
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {

        
    }
    
    
   /* // ALLOWS FOR ALERT POPUP
    func webView(_ webView: WKWebView,
                 runJavaScriptAlertPanelWithMessage message: String,
                 initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping () -> Void) {
        
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let title = NSLocalizedString("OK", comment: "OK Button")
        let ok = UIAlertAction(title: title, style: .default) { (action: UIAlertAction) -> Void in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(ok)
        present(alert, animated: true)
        completionHandler()
    }*/
    
    //TRIGGERED WHEN WEBVIEW FINISHED LOADING
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("finished loading \(NSDate())")

    }
    
    //TRIGGERED IF WEBVIEW LOADING FAILED
    func webView(_ webView: WKWebView,
                      didFailProvisionalNavigation navigation: WKNavigation!,
                      withError error: Error) {
        print("Felmeddelande: \(error)")
        print("Entered error catch")
        
        if (error as NSError).code == -1001 { // TIMED OUT:
            
            // CODE to handle TIMEOUT
            
        } else if (error as NSError).code == -1009 { // NO INTERNET CONNECTION
            
        } else if (error as NSError).code == -1003 { // SERVER CANNOT BE FOUND
            
            // CODE to handle SERVER not found
            
        } else if (error as NSError).code == -1100 { // URL NOT FOUND ON SERVER
            
            // CODE to handle URL not found
            
        } else {
        }
    }

    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        //KLARNA
        klarnaSDK?.newPageLoad(in: logicWebView)
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        //KLARNA
        if klarnaSDK.shouldFollowNavigation(withRequest: navigationAction.request) != true {
            decisionHandler(.cancel)
        }
            //If bankid:/// URL:
        else if navigationAction.request.url?.scheme == "bankid" {
            UIApplication.shared.open(navigationAction.request.url!)
            decisionHandler(.cancel)
        }
        //If swish:/// URL:
        else if navigationAction.request.url?.scheme == "swish" {
             UIApplication.shared.open(navigationAction.request.url!)
             decisionHandler(.cancel)
         } else {
             decisionHandler(.allow)
         }
    }
}*/

