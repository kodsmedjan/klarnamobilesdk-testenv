//
//  ViewController.swift
//  AlfaGoOn-iOS
//
//  Created by Amy Niklasson on 2018-11-29.
//  Copyright © 2018 Amy Niklasson. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation
import SystemConfiguration.CaptiveNetwork
import WebKit
import KlarnaMobileSDK

/*class FirstViewController: UIViewController, WKScriptMessageHandler,
WKUIDelegate, WKNavigationDelegate, KlarnaHybridSDKEventListener {
    func klarnaHybridSDKWillShowFullscreen(inWebView webView: KlarnaWebView, completion: @escaping () -> Void) {
        print("KlarnaTest")

    }
    
    func klarnaHybridSDKDidShowFullscreen(inWebView webView: KlarnaWebView, completion: @escaping () -> Void) {
        print("KlarnaTest")

    }
    
    func klarnaHybridSDKWillHideFullscreen(inWebView webView: KlarnaWebView, completion: @escaping () -> Void) {
        print("KlarnaTest")

    }
    
    func klarnaHybridSDKDidHideFullscreen(inWebView webView: KlarnaWebView, completion: @escaping () -> Void) {
        print("KlarnaTest")

    }
    
    func klarnaHybridSDKFailed(inWebView webView: KlarnaWebView, withError error: KlarnaMobileSDKError) {
        print("KlarnaTest")

    }
    
    
    

    
    var logicWebView : WKWebView!
    let url = URL (string: "https://app.klt.kodsmedjan.se/GoOn/amybeta/payment/paymentSettings.php")
    var webReady = false
    var appInBackground = false
    var viewShowing = true
    let uid = "KlarnaSDKtest"
    var klarnaSDK:KlarnaHybridSDK!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //SETUP CONTENTCONTROLLER (Connection between swift, webview and webpage)
        let contentController = WKUserContentController()
        contentController.add(self, name: "serviceRequest")
        let config = WKWebViewConfiguration()
        config.userContentController = contentController
        
        //SETUP WKWEBVIEW
        logicWebView = WKWebView(frame: self.view.frame, configuration: config)
        //Required for webView to work at all
        logicWebView?.uiDelegate = self
        //Required for func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!)
        logicWebView?.navigationDelegate = self
        
        self.view.addSubview(logicWebView!)

        //KLARNA
        KlarnaMobileSDKCommon.setLoggingLevel(.verbose)
        klarnaSDK = KlarnaHybridSDK(webView: logicWebView, returnUrl: URL(string: "kms://")!, eventListener: self as KlarnaHybridSDKEventListener)
        
        // SETUP URL PUT REQUEST AND LOAD IT
        // To call the url without using any cache. To call with cache change to: URLRequest(url: url!)
        var urlRequest = URLRequest.init(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 100)
        
        urlRequest.httpMethod = "PUT"
        let postString = "id=\( uid )"
        urlRequest.httpBody = postString.data(using: .utf8)
        logicWebView?.load(urlRequest)
        

    }

    //TRIGGERED WHEN RECEIVED TRIGGER FROM WEBVIEW
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
    }
    
    
    //TRIGGERED IF WEBVIEW LOADING FAILED
    func webView(_ webView: WKWebView,
                 didFailProvisionalNavigation navigation: WKNavigation!,
                 withError error: Error) {
        print("Felmeddelande: \(error)")
        
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        //KLARNA
        klarnaSDK?.newPageLoad(in: logicWebView)
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        //KLARNA
        let shouldFollow = klarnaSDK.shouldFollowNavigation(withRequest: navigationAction.request)
        decisionHandler(shouldFollow ? WKNavigationActionPolicy.allow : WKNavigationActionPolicy.cancel)
    }
    
}*/



