//
//  SettingsViewController.swift
//  AlfaGoOn-iOS
//
//  Created by Amy Niklasson on 2019-12-11.
//  Copyright © 2019 Amy Niklasson. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation
import WebKit
import KlarnaMobileSDK

class SecondViewController: UIViewController, WKScriptMessageHandler,
WKUIDelegate, WKNavigationDelegate, KlarnaHybridEventListener {
    func klarnaWillShowFullscreen(inWebView webView: KlarnaWebView, completionHandler: @escaping () -> Void) {
        print("KlarnaTest")
    }
    
    func klarnaDidShowFullscreen(inWebView webView: KlarnaWebView, completionHandler: @escaping () -> Void) {
        print("KlarnaTest")
    }
    
    func klarnaWillHideFullscreen(inWebView webView: KlarnaWebView, completionHandler: @escaping () -> Void) {
        print("KlarnaTest")
    }
    
    func klarnaDidHideFullscreen(inWebView webView: KlarnaWebView, completionHandler: @escaping () -> Void) {
        print("KlarnaTest")
    }
    
    func klarnaFailed(inWebView webView: KlarnaWebView, withError error: KlarnaMobileSDKError) {
        print("KlarnaTest")
    }
    
    //TRIGGERED WHEN RECEIVED TRIGGER FROM WEBVIEW
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
    }

    
    var logicWebView : WKWebView!
    let url = URL (string: "https://app.klt.kodsmedjan.se/GoOn/amybeta/payment/paymentSettings.php")
    var webReady = false
    var appInBackground = false
    var viewShowing = true
    let uid = "KlarnaSDKtest"
    var klarnaSDK:KlarnaHybridSDK!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //SETUP CONTENTCONTROLLER
        let contentController = WKUserContentController()
        contentController.add(self, name: "serviceRequest")
        let config = WKWebViewConfiguration()
        config.userContentController = contentController
        
        //SETUP WKWEBVIEW
        logicWebView = WKWebView(frame: self.view.frame, configuration: config)
        logicWebView?.uiDelegate = self
        logicWebView?.navigationDelegate = self
        
        self.view.addSubview(logicWebView!)

        //KLARNA
        KlarnaMobileSDKCommon.setLoggingLevel(.verbose)
        klarnaSDK = KlarnaHybridSDK(returnUrl: URL(string: "kms://")!, eventListener: self )
        klarnaSDK.addWebView(self.logicWebView)
        
        // SETUP URL PUT REQUEST AND LOAD IT
        var urlRequest = URLRequest.init(url: url!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 100)
        
        urlRequest.httpMethod = "PUT"
        let postString = "id=\( uid )"
        urlRequest.httpBody = postString.data(using: .utf8)
        logicWebView?.load(urlRequest)
        

    }
    
    //TRIGGERED IF WEBVIEW LOADING FAILED
    func webView(_ webView: WKWebView,
                 didFailProvisionalNavigation navigation: WKNavigation!,
                 withError error: Error) {
        print("Felmeddelande: \(error)")
        
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        //KLARNA
        klarnaSDK?.newPageLoad(in: logicWebView)
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        //KLARNA
        let shouldFollow = klarnaSDK.shouldFollowNavigation(withRequest: navigationAction.request)
        decisionHandler(shouldFollow ? WKNavigationActionPolicy.allow : WKNavigationActionPolicy.cancel)
    }
    
}

