**Steps to reproduce issue where the iframe/popup does not appear**
1.  Go to tab "Second" unless already there.
2.  Press the button "Ändra"
3.  Mark the Klarna logo by clicking on it
4.  Press the button "Nästa"
5.  Choose the alternative "Kort"
6.  Fill in the following "Kortnummer": 4111 1111 1111 1111
7.  Fill in the following "MM/ÅÅ": 12/25
8.  Fill in the following "CVC": 123
9.  Scroll down to the bottom of the page
10.  Press the button "Nästa"
11.  At this stage the popup/iframe should appear but it does not.